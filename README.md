## QSemOS的IDE插件公共配置文件目录结构如下：
+ 公用配置目录在：[用户目录]./.gba-ncti/
```bash
├─packages              依赖运行包，如页面包webapp
├─reps                  
│  ├─board              板卡信息，boards-profile.json为板卡索引文件，子目录为与板卡id对应的板卡子目录
│  ├─bsd                板级支持包，bsps-profile.json为索引文件，主要在yocto工程时使用
│  ├─distor             发行版内容，distors-profile.json为索引文件，放置linux，rtos，rootfs等内容
│     ├─embedded_img    嵌入式镜像包，如qemu使用的标准arm平台的linux内核镜像和rootfs文件系统
│  ├─pkg                运行软件包，pkgs-profile.json为索引文件，放置rpm等预设软件包内容
│  ├─sdk                软件开发工具包，sds-profile.json为索引文件，放置sdk,lib等内容,sdk指明已下载的SDK，与板卡关联，安装后显示已安装的包
│  ├─soft               应用软件包，softs-profile.json为索引文件，放置三方应用软件包内容，当前插件未用到
│  └─template           模板目录，templates-profile.json为索引文件，放置各类工程的模板内容
│     ├─ai-code-templates        AI高效代码模板内容目录
│     ├─bootloader               bootload模板目录
│     ├─C                        C工程模板目录
│     ├─C_ko                     C驱动工程模板目录
│     ├─C++                      C++工程模板目录
│     ├─C++_ko                   C++驱动工程模板目录
│     ├─C-C++                    C及C++混合工程模板目录
│     ├─general                  通用工程模板目录
│     ├─jailhouse                混合部署jailhouse相关模板目录
│     ├─meta-data                自动填充元数据相关模板目录
│     ├─ai-code-templates.json   AI高效代码模板独立索引文件
│     ├─meta-data-templates.json 后面将元数据的模板拆出成独立索引文件
│
│  settings.json       各插件使用的配置参数
│  homestate.json      插件使用的环境变更或资源清单等
│  README.md
```

## 字段含义说明：
### 板卡信息，boards-profile.json
```json
"id":板卡ID唯一标识，如：ok3568,后面如模板关联中会使用到，
"name":板卡名称, "company":厂商,"platform":平台，如：arm,arm64,x86等,"framework":架构,
"rom":运行内存容量,"ram":存储容量,"hybrid":支持的混合部署架构，如：["Jailhouse","Xvisor"],"directions":描述
```

### 板级支持包，bsps-profile.json
+ 分了：yocto：包，layer：层
```json
"id":ID唯一标识,"name":名称,"company":厂商,"platform":平台，如：arm,arm64,x86等,
"url": 下载地址，如：https://gitee.com/openeuler/rockchip-kernel.git,
"subdir": 下载地址如是git的子目录,
"branch": 下载地址如是git的分支，如：openEuler-22.03-LTS-SP3,
"directions": 描述
```

### 发行版内容，distors-profile.json
+ 分了类型：yocto-os：yocto系统，linux-kernel：linux内核，rtos：硬实时系统，rootfs：文件系统
```json
"id": 发行版ID唯一标识,
"name": 名称,
"company": 厂商,
"platform": 平台，如：arm,arm64,x86等,
"url": 下载地址，如：https://gitee.com/openeuler/rockchip-kernel.git,
"subdir": 下载地址如是git的子目录,
"branch": 下载地址如是git的分支，如：openEuler-22.03-LTS-SP3,
"directions": 描述
```

### 运行软件包，pkgs-profile.json
+ 分了类型：rpm,deb
```json
"rpm_source_type": 包来源类型，如：local,http,https,ftp,ftps, "rpm_name": 包名称,
"platform": 平台，如：arm,arm64,x86等,
"rpm_path_url": 包路径，可以是远程，也可是本地，现使用的场景是本地,"rpm_checked": 是否默认勾选，如：false,
"rpm_run_path": 包运行路径，如：/usr/local/ok3568/arm/ok3568/1.0.0,
"rpm_ver": 版本,如:1.0.0
```

### 软件开发工具包，sds-profile.json
+ 分了类型：sdk,lib
- sdk:
```json
"sdk_down_type": 来源类型，如：local,http,https,ftp,ftps, "sdk_name": 名称,
"platform": 平台，如：arm,arm64,x86等,
"sdk_path_url": sdk路径，可以是远程，也可是本地，现使用的场景是本地,"sdk_checked": 是否默认勾选，如：false,
"sdk_run_path": sdk安装后运行路径，如：/usr/local/sdk/gcos-glibc-x86_64-openeuler-image-aarch64-ok3568-toolchain-V1R1C0,
"boards": 对应板卡，如：ok3568,
"sdk_ver": 版本,如:1.0.0
```
- lib:
```json
"id": 软件包ID唯一标识,
"name": 名称,
"company": 厂商,
"platform": 平台，如：arm,arm64,x86等,
"url": 下载地址，如：URL_ADDRESS
"subdir": 下载地址如是git的子目录,
"branch": 下载地址如是git的分支，如：openEuler-22.03-LTS-SP3,
"directions": 描述,
"ver":版本
```

### 模板，templates-profile.json
+ 模板templates-profile中的分类：
general             通用
linux-kernel        linux内核
c                   c模板
meta-data           元数据模板，下面又可分几个类型
   linux-kernel      linux内核
   hybrid-deployment 混合部署
   hard-realtime     硬实时类型

```json
"id": 模板ID唯一标识,
"name": 名称,
"company": 厂商,
"boards": 对应板卡，如：ok3568；all为支持全部,
"url": 下载地址，local时为本地,
"subdir": 本地模板位置,云地址时为git子目录
"branch": 云地址时为git分支,
"directions": 描述
```
