#include <linux/module.h>    /* Needed by all modules */
#include <linux/kernel.h>    /* Needed for KERN_INFO */
#include <linux/init.h>        /* Needed for the macros */

#define AUTHOR "GBA CROSSING"
#define DESCRIPTION "第一个驱动模板"
#define SUPPORTED "Test Device"

MODULE_LICENSE ("Dual BSD/GPL");
MODULE_AUTHOR (AUTHOR);
MODULE_DESCRIPTION (DESCRIPTION);
MODULE_SUPPORTED_DEVICE (SUPPORTED);


/* 驱动入口函数 */
static int __init mymodule_init(void)
{
	/* 入口函数具体内容 */
	printk(KERN_INFO "Hello, world\n");
	int retvalue = 0;
	/* 注册字符设备驱动 */
	retvalue = register_chrdev(200, "chrtest", &test_fops);
	if(retvalue < 0){
		/* 字符设备注册失败,自行处理 */
	}
	return 0;
}

/* 驱动出口函数 */
static void __exit mymodule_exit(void) {
	printk(KERN_INFO "Goodbye, world\n");
	/* 注销字符设备驱动 */
	unregister_chrdev(200, "chrtest");
}


module_init(mymodule_init);
module_exit(mymodule_exit);

