C工程，工程目录结构如下
├─.vscode
├─bin        			存放二进制文件
├─build     			存放构建文件
│      CMakeLists.txt	cmake构建文档
│
├─docs      			文档目录
├─include   			头文件目录
│  └─inner
├─lib       			库文件目录
├─src      				源码文件目录
│      main.c  			源码文件
│
└─test
│  .gitignore
│  Makefile            make文件
│  README.md
