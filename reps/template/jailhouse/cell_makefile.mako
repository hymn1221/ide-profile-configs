# 编译cell文件
# src为jailhouse的主目录，可通过$(J_SRC)引用
# CROSS_COMPILE指定交叉编译工具，ARCH 指定目标平台

J_SRC ?= ../src
# includes jailhouse installation-related variables and definitions
include $(J_SRC)/scripts/include.mk

CC ?= gcc
OBJCOPY=objcopy
 
ARCH ?= arm64
 
ifdef CROSS_COMPILE
CC := $(CROSS_COMPILE)gcc
OBJCOPY=$(CROSS_COMPILE)objcopy
endif
 
INCLUDE_DIRS =  -I$$PWD/$(J_SRC)/hypervisor/arch/$(ARCH)/include \
		-I$$PWD/$(J_SRC)/hypervisor/include \
		-I$$PWD/$(J_SRC)/include
 
 
OBJDIR := objs/$(ARCH)
SRC=$(wildcard *.c)
OBJECTS:=$(patsubst %.c,%.o,$(SRC))
CELL:=$(patsubst %.c,%.cell,$(SRC))
 
 
 
all: MD  $(CELL)
 
 
 
OBJFLAGS = -O binary 
#--remove-section=.note.gnu.property
 
$(CELL) : %.cell : %.o
	$(OBJCOPY)  $(OBJFLAGS)  $(OBJDIR)/$<  $@ 
	@echo "生成cell完成，$(OBJDIR)" 
 
 
$(OBJECTS)  : %.o : %.c
	$(CC) -c  $<  -o $(OBJDIR)/$@  $(INCLUDE_DIRS) 
	@echo "编译.c生成.o完成，$(OBJDIR)"
 
 
MD:
	mkdir -p  $(OBJDIR)
 
.PHONY : clean
 
clean:
	rm -rf $(CELL) $(OBJDIR)/$(OBJECTS)
 