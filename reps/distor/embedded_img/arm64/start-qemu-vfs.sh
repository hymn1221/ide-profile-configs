echo "启动qemu aarch64 的openeuler-22.09镜像,共享文件系统/tmp/shard/"
qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic \
    -kernel zImage \
    -initrd openeuler-image-qemu-aarch64*.rootfs.cpio.gz \
    -device virtio-9p-device,fsdev=fs1,mount_tag=host \
    -fsdev local,security_model=passthrough,id=fs1,path=/tmp/shard
