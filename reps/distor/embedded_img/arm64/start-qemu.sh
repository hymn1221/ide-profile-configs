echo "启动标准qemu aarch64 的openeuler-22.09镜像"
echo "windows下启动'C:\Program Files\Git\bin\bash.exe'来启动"
qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic \
    -kernel zImage \
    -initrd openeuler-image-qemu-aarch64*.rootfs.cpio.gz
