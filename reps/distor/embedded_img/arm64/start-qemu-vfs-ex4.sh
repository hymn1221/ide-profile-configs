echo "启动qemu aarch64 的openeuler-22.09镜像,rootfs.ext4,共享文件系统/tmp/shard/"
qemu-system-aarch64 -M virt-4.0 -m 2G -cpu cortex-a57 -nographic \
    -kernel zImage \
    -append "rootwait root=/dev/vda rw console=ttyAMA0" \
    -netdev user,id=eth0 \
    -device virtio-net-device,netdev=eth0 \
    -drive file=rootfs.ext4,if=none,format=raw,id=hd0 \
    -device virtio-blk-device,drive=hd0
