echo "启动标准qemu arm 的openeuler-22.09镜像"
qemu-system-arm -M virt -m 1G -cpu cortex-a53 -nographic \
    -kernel zImage \
    -initrd openeuler-image-qemu-arm*.rootfs.cpio.gz
